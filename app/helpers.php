<?php

function latlngToArray($object) {
  $array1 = '[';
  if (is_string($object)) {
    $array_of_object = json_decode($object);
    foreach ($array_of_object as $key => $object) {
      $array1 .= '[' . $object->lat . ',' . $object->lng . '],' ;
    }
  }
  $array1 = substr($array1,0,-1);
  $array1 .= ']';
  echo $array1;
}

function splitquoters($string){
  if (strpos($string, "'") !== false) {
    $string =  str_replace("'", "\\\'", $string);
    return $string;
  }
  return $string;
}
