<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layer extends Model
{
  protected $fillable = ['name', 'color', 'discription', 'created_at', 'updated_at'];

  public function geo()
    {
        return $this->hasMany('App\Geo');
    }
}
