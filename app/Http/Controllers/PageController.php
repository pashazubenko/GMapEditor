<?php

namespace App\Http\Controllers;

use App\User;
use App\Layer;
use App\Geo;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Show index page
     * @param  Request
     */
    public function index(Request $request)
    {
      $layers = explode(",", $request->layers);
      $hide = explode(",", $request->hide);
      $data = array();

      if (!empty($request->kadastrMap)) {
        $data['kadastrMap'] = url()->full();
        $data['kadastrMap'] = str_replace('kadastrMap=1&','',$data['kadastrMap']);
      }

      if (!empty($layers[0])) {
        if (!empty($hide[0])) {

        }
        $data['zones'] = collect();
        $data['objects'] = collect()  ;
        foreach ($layers as $layer) {
          $data['zones'] = $data['zones']->merge(Geo::orderBy('created_at', 'desc')->where('layer_id', $layer)->where('type', 'zone')->get());
          $data['objects'] = $data['objects']->merge(Geo::orderBy('created_at', 'desc')->where('layer_id', $layer)->where('type', 'object')->get());
        }
      }
      else{
        $data['zones'] = Geo::orderBy('created_at', 'desc')->where('type', 'zone')->get();
        $data['objects'] = Geo::orderBy('created_at', 'desc')->where('type', 'object')->get();
      }

      $data['layers'] = Layer::orderBy('created_at', 'desc')->get();
      return response()->view('pages.index', $data, 200);
    }

    /**
     * Show integrated map
     */
    public function innerMap()
    {
        $data['layers'] = Layer::orderBy('created_at', 'desc')->get();
        $data['zones'] = Geo::orderBy('created_at', 'desc')->where('type', 'zone')->get();
        $data['objects'] = Geo::orderBy('created_at', 'desc')->where('type', 'object')->get();

        return response()->view('pages.innerMap', $data, 200);
    }

    /**
     * Show custom map
     * @param  Request
     */
    public function buildMap(Request $request)
    {
        $requestData = json_decode($request->data, true);
        $layers = [];
        $data['request'] = $request->data;
        $data['zones'] = collect();
        $data['objects'] = collect();

        if ($request->show) {
          $data['zones'] = $data['zones']->merge(Geo::orderBy('created_at', 'desc')->where('layer_id', $request->show)->where('type', 'zone')->take(1500)->get());
          $data['objects'] = $data['objects']->merge(Geo::orderBy('created_at', 'desc')->where('layer_id', $request->show)->where('type', 'object')->take(1500)->get());
        }
        elseif ($request->data) {
          foreach ($requestData as $key => $reqdata) {
            foreach ($reqdata['layers'] as $key2 => $layer) {
              $data['zones'] = $data['zones']->merge(Geo::orderBy('created_at', 'desc')->where('layer_id', $layer['val'])->where('type', 'zone')->take(1500)->get());
              $data['objects'] = $data['objects']->merge(Geo::orderBy('created_at', 'desc')->where('layer_id', $layer['val'])->where('type', 'object')->take(1500)->get());
            }
          }
          $data['layers'] = Layer::find($layers);
        }
        else {
          $data['layers'] = Layer::orderBy('created_at', 'desc')->get();
          $data['zones'] = Geo::orderBy('created_at', 'desc')->where('type', 'zone')->get();
          $data['objects'] = Geo::orderBy('created_at', 'desc')->where('type', 'object')->get();
        }

        if ($request->kadastrMap) {
          $data['kadastrMap'] = $request->kadastrMap;
        }
        else{
          $data['kadastrMap'] = null;
        }
        return response()->view('pages.buildMap', $data, 200);
    }
}
