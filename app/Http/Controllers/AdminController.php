<?php

namespace App\Http\Controllers;

use App\Geo;
use App\Layer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['layers'] = Layer::orderBy('created_at', 'desc')->take(5)->get();
        $data['zones'] = Geo::orderBy('created_at', 'desc')->where('type', 'zone')->take(5)->get();
        $data['objects'] = Geo::orderBy('created_at', 'desc')->where('type', 'object')->take(5)->get();
        $data['objects_count'] = Geo::where('type', 'object')->count();
        $data['layers_count'] = Layer::count();
        $data['zones_count'] = Geo::where('type', 'zone')->count();

        return response()->view('admin.index', $data, 200);
    }

    public function builderPage()
    {
        $layers = Layer::orderBy('created_at', 'desc')->get();
        $data['layers'] = array();
        foreach ($layers as $layer) {
            $array = array('label' => $layer->name, 'val' => $layer->id);
            array_push($data['layers'],$array);
        }
        $data['layers'] = json_encode($data['layers']);
        return response()->view('admin.builder', $data);
    }

    public function listAll($type)
    {
        switch ($type) {
            case 'layer':
                $data['layers'] = Layer::orderBy('created_at', 'desc')->get();
                $data['layers_count'] = Layer::count();
                break;

            case 'zone':
                $data['zones'] = Geo::orderBy('created_at', 'desc')->where('type', 'zone')->get();
                $data['zones_count'] = Geo::where('type', 'zone')->count();
                break;

            case 'object':
                $data['objects_count'] = Geo::where('type', 'object')->count();
                $data['objects'] = Geo::orderBy('created_at', 'desc')->where('type', 'object')->get();
                break;
        }
        return response()->view('admin.list-all', $data, 200);
    }

    public function editLayerPage($id = null)
    {
        $data['request'] = null ;
        if (!empty($id)) {
            if ($layer = Layer::where('id', $id)->first()) {
                $data['request'] = [
                'name' => $layer->name,
                'color' => $layer->color,
                'discription' => $layer->discription,
                'id' => $id,
                ];
            }
        }
        return response()->view('admin.layer-edit', $data, 200);
    }

    public function editPolygonPage($id = null)
    {
      if (!empty($id)) {
          if ($layer = Geo::where('id', $id)->first()) {
              $data['request'] = [
            'name' => $layer->name,
            'layer' => $layer->layer,
            'discription' => $layer->discription,
            'geoData' => $layer->geoData,
            'id' => $id,
            ];
          }
      }

        $data['layers'] = Layer::orderBy('created_at', 'desc')->get();
        return response()->view('admin.polygon-edit', $data, 200);
    }

    public function editPointPage($id = null)
    {
      if (!empty($id)) {
          if ($layer = Geo::where('id', $id)->first()) {
              $data['request'] = [
            'name' => $layer->name,
            'layer' => $layer->layer,
            'discription' => $layer->discription,
            'geoData' => $layer->geoData,
            'id' => $id,
            ];
          }
      }

        $data['layers'] = Layer::orderBy('created_at', 'desc')->get();
        return response()->view('admin.object-edit', $data, 200);
    }

    public function editLayer(Request $request)
    {
        $data['request'] = $request->all();

        if ($this->layerDataValidator($request->all())->fails()) {
            $data['errors'] = $this->layerDataValidator($request->all())->errors();
            return response()->view('admin.layer-edit', $data, 200);
        }

        $layer = $this->editOrCreateLayer($request);

        return redirect('admin/listall/layer')->with('message', 'Cлой ' . $layer->id . ' успешно обработан');
    }

    public function editPolygon(Request $request)
    {
        $data['layers'] = Layer::orderBy('created_at', 'desc')->take(15)->get();

        if ($this->geoDataValidator($request->all())->fails()) {
            $data['request'] = $request->all();
            $data['errors'] = $this->geoDataValidator($request->all())->errors();
            return response()->view('admin.polygon-edit', $data, 200);
        }

        $layer = $this->editOrCreateGeo($request);

        return redirect('admin/listall/zone')->with('message', 'Зона ' . $layer->id . ' успешно обработана');
    }

    public function editPoint(Request $request)
    {

        $data['layers'] = Layer::orderBy('created_at', 'desc')->take(50)->get();

        if ($this->geoDataValidator($request->all())->fails()) {
            $data['request'] = $request->all();
            $data['errors'] = $this->geoDataValidator($request->all())->errors();
            return response()->view('admin.object-edit', $data, 200);
        }

        $point = $this->editOrCreatePoint($request);

        $data['done'] = $point->id;
        return redirect('admin/listall/object')->with('message', 'Обьект ' . $point->id . ' успешно обработан');
    }



    private function editOrCreateGeo($request)
    {
        if ($geo = Geo::where('id', $request->id)->first()) {
            $geo->name = $request->name;
            $geo->type = 'zone';
            $geo->layer_id = $request->layer;
            $geo->discription = $this->clearDiscription($request->discription);
            $request->geoData = str_replace('[[', '[', $request->geoData);
            $geo->geoData = str_replace(']]', ']', $request->geoData);
            $geo->save();
            return $geo;
        }

        $geo = new Geo;
        $geo->name = $request->name;
        $geo->type = 'zone';
        $geo->layer_id = $request->layer;
        $geo->discription = $this->clearDiscription($request->discription);
        $request->geoData = str_replace('[[', '[', $request->geoData);
        $geo->geoData = str_replace(']]', ']', $request->geoData);
        $geo->save();
        return $geo;
    }

    private function editOrCreatePoint($request)
    {
        if ($geo = Geo::where('id', $request->id)->first()) {
            $geo->name = $request->name;
            $geo->type = 'object';
            $geo->layer_id = $request->layer;
            $geo->icon = $request->icon;
            $geo->discription = $this->clearDiscription($request->discription);
            $request->geoData = str_replace('[[', '[', $request->geoData);
            $geo->geoData = str_replace(']]', ']', $request->geoData);
            $geo->save();
            return $geo;
        }

        $geo = new Geo;
        $geo->name = $request->name;
        $geo->type = 'object';
        $geo->layer_id = $request->layer;
        $geo->icon = $request->icon;
        $geo->discription = $this->clearDiscription($request->discription);
        $request->geoData = str_replace('[[', '[', $request->geoData);
        $geo->geoData = str_replace(']]', ']', $request->geoData);
        $geo->save();
        return $geo;
    }

    private function editOrCreateLayer($request)
    {
        if ($layer = Layer::where('id', $request->id)->first()) {
            $layer->name = $request->name;
            $layer->color = $request->color;
            $layer->discription = $request->discription;
            $layer->save();
            return $layer;
        }

        $layer = new Layer;
        $layer->name = $request->name;
        $layer->color = $request->color;
        $layer->discription = $request->discription;
        $layer->save();

        return $layer;
    }


    private function geoDataValidator(array $data)
    {
        return Validator::make($data, [
                'name' => 'required|max:255|min:6',
                'layer' => 'required|',
                'discription' => 'required|min:6',
                'geoData' => 'required|min:6',
            ]);
    }

    private function layerDataValidator(array $data)
    {
        return Validator::make($data, [
                'name' => 'required|max:255|min:6',
                'color' => 'required|max:6|min:6',
                'discription' => 'required'
        ]);
    }

    private function clearDiscription($discription)
    {
        $discription =  str_replace("\r", "", $discription);
        $discription =  str_replace("\n", "", $discription);
        $discription =  str_replace("\t", "", $discription);
        return $discription;
    }

    public function delete($type, $id)
    {
        $data['layers'] = Layer::orderBy('created_at', 'desc')->take(5)->get();
        $data['zones'] = Geo::orderBy('created_at', 'desc')->where('type', 'zone')->take(5)->get();
        $data['objects'] = Geo::orderBy('created_at', 'desc')->where('type', 'object')->take(5)->get();

        switch ($type) {
            case 'layer':
                $layer = Layer::where('id', $id)->first();
                foreach ($layer->geo as $geo) {
                    $geo->delete();
                }
                $layer->delete();
                break;

            case 'zone':
                Geo::where('id', $id)->delete();
                break;

            case 'object':
                Geo::where('id', $id)->delete();
                break;
        }

        return back()->with('warning', 'Был удален: ' . $type . ' ' . $id);
    }
}
