<?php

namespace App\Http\Controllers;

use App\Geo;
use App\Layer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $type, $params)
    {
        $data = [];
        switch ($type) {
            case 'layers':
                $data = Layer::orderBy('created_at', 'desc')->get();
                break;
            case 'zones':
                $data = Geo::orderBy('created_at', 'desc')->where('type', 'zone')->get(['geoData']);
                break;
            case 'objects':
                $data = Geo::orderBy('created_at', 'desc')->where('type', 'object')->get();
                break;

            default:
                $data = 'not_found';
                break;
        }
        return response()->json( $data, 200);
    }

}
