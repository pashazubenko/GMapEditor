<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Geo extends Model
{
    protected $table = 'geos';

    public function layer()
    {
        return $this->belongsTo('App\Layer');
    }
}
