<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/*
 *  Pages
 */
Route::get('/',      'PageController@index');
Route::get('/inner', 'PageController@innerMap');
Route::get('/build', 'PageController@buildMap');

/*
 *  Admin menu
 */
Route::prefix('admin')->group(function () {
  Route::get('/',                'AdminController@index');
  Route::get('/builder',         'AdminController@builderPage');
  Route::get('/listall/{type}',  'AdminController@listAll');

  Route::get('/del/{type}/{id}', 'AdminController@delete');

  Route::get('/edit-layer',      'AdminController@editLayerPage');
  Route::get('/edit-layer/{id}', 'AdminController@editLayerPage');
  Route::post('/edit-layer',     'AdminController@editLayer');

  Route::get('/edit-geo',        'AdminController@editPolygonPage');
  Route::get('/edit-geo/{id}',   'AdminController@editPolygonPage');
  Route::post('/edit-geo',       'AdminController@editPolygon');

  Route::get('/edit-point',      'AdminController@editPointPage');
  Route::get('/edit-point/{id}', 'AdminController@editPointPage');
  Route::post('/edit-point',     'AdminController@editPoint');
});
