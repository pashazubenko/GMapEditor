$( ".colorpic" ).click(function() {
  var color = $( this ).text();
   $('#colorpic').val(color);
});

var scriptjs = require('scriptjs');
scriptjs('/js/ckeditor/ckeditor.js', function(){
    CKEDITOR.replace('discription', { toolbar : [ [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink','-', 'Image', 'Source' ], ]});
});

$('.delete-message').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var id = button.data('id') ;
  var otype = button.data('otype') ;
  var modal = $(this);
  if (otype == 'layer') {
    modal.find('.modal-body').html('Вы уверены что хотите удалить слой #' + id + '</br> <b>ВНИМАНИЕ!</b> Это приведет к удалению обьектов слоя!' );
  }
  else{
    modal.find('.modal-body').text('Вы уверены что хотите удалить ' + otype + ' ' + id);
  }
  modal.find('.del').click(function() {
    window.location.href = '/admin/del/' + otype + '/' + id;
  });
})
