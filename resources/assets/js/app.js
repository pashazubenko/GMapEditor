import { Slider } from 'vue-color';
import vSelect    from 'vue-select';

require('./bootstrap');

window.Vue               = require('vue');
window.VueGoogleMaps     = require('vue2-google-maps');
window.Vue2Leaflet       = require('vue2-leaflet');
window.$ = window.jQuery = require('jquery');

Vue.component('google-map',              require('./components/google-map.vue'));
Vue.component('google-map-marker',       require('./components/google-map-marker.vue'));
Vue.component('google-map-polygon',      require('./components/google-map-polygon.vue'));
Vue.component('google-map-layer-editor', require('./components/google-map-layer-editor.vue'));
Vue.component('google-map-point-editor', require('./components/google-map-point-editor.vue'));
Vue.component('leaflet-map',             require('./components/leaflet-map.vue'));
Vue.component('menu-builder',            require('./components/menu-builder.vue'));

Vue.component('v-map',         Vue2Leaflet.Map);
Vue.component('v-tilelayer',   Vue2Leaflet.TileLayer);
Vue.component('v-marker',      Vue2Leaflet.Marker);
Vue.component('v-polygon',     Vue2Leaflet.Polygon);
Vue.component('v-popup',       Vue2Leaflet.Popup);
Vue.component('wms-tilelayer', Vue2Leaflet.WMSTileLayer);
Vue.component('v-select',      vSelect);
Vue.component('slider-picker', Slider);

var degrees2meters = function(lon,lat) {
        var x = lon * 20037508.34 / 180;
        var y = Math.log(Math.tan((90 + lat) * Math.PI / 360)) / (Math.PI / 180);
        y = y * 20037508.34 / 180;
        return [x, y]
}

const app = new Vue({
    el: '#app',

    data: {
      cats: [],
      googleShow: true,
      kadastrShow: false,
      center: {lat: 46.96153979413719, lng: 31.98896884918213},

      //icons
      accomodation: L.icon({ iconUrl: 'images/pin/accomodation.png', popupAnchor: [16, -1] }),
      electricity:  L.icon({ iconUrl: 'images/pin/electricity.png',  popupAnchor: [16, -1] }),
      gas:          L.icon({ iconUrl: 'images/pin/gas.png',          popupAnchor: [16, -1] }),
      industrial:   L.icon({ iconUrl: 'images/pin/industrial.png',   popupAnchor: [16, -1] }),
      recreation:   L.icon({ iconUrl: 'images/pin/recreation.png',   popupAnchor: [16, -1] }),
      sewage:       L.icon({ iconUrl: 'images/pin/sewage.png',       popupAnchor: [16, -1] }),
      water:        L.icon({ iconUrl: 'images/pin/water.png',        popupAnchor: [16, -1] }),
    },

    methods:{
      updateColorValue (value) {
        this.$refs.colorpic.value = value.hex.replace('#', '');
      },
      showOrHide(id) {
        if (this.cats.length > 0) {
          return this.cats.includes(id)
        }
        else{
          return true
        }
      },
      kadastrMap($refs) {
        if (this.kadastrShow == true) {
          map = this.$refs.gmap.$refs.map.$mapObject
          map.overlayMapTypes.clear()
          this.kadastrShow = false
        }
        else{
          this.kadastrShow = true
          map = this.$refs.gmap.$refs.map.$mapObject
          console.log(map);
          var WMSLayer = new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                var proj = map.getProjection();
                var zfactor = Math.pow(2, zoom);
                var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 256 / zfactor, coord.y * 256 / zfactor));
                var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 256 / zfactor, (coord.y + 1) * 256 / zfactor));
                var bbox = (degrees2meters(top.lng(),bot.lat())) + "," + degrees2meters(bot.lng(),top.lat());
                var url = "http://map.land.gov.ua/geowebcache/service/wms?tiled=true";
                    url += "&REQUEST=GetMap"; //WMS operation
                    url += "&SERVICE=WMS";    //WMS service
                    url += "&VERSION=1.1.1";  //WMS version
                    url += "&LAYERS=kadastr"; //WMS layers
                    url += "&FORMAT=image/png" ; //WMS format
                    url += "&TRANSPARENT=true";
                    url += "&SRS=EPSG:3857";     //set EPSG:3857
                    url += "&BBOX=" + bbox;      // set bounding box
                    url += "&WIDTH=256";         //tile size in google
                    url += "&HEIGHT=256";
                return url;                 // return URL for the tile
            },
            tileSize: new google.maps.Size(256, 256),
          });
          map.overlayMapTypes.push(WMSLayer);
        }
      },
      setPlace(place) {
        this.center = {
          lat: place.geometry.location.lat(),
          lng: place.geometry.location.lng(),
        }
      }
    }
});

// colorpic @TODO
$( ".colorpic" ).click(function() {
  var color = $( this ).text();
   $('#colorpic').val(color);
});

// CKEDITOR
var scriptjs = require('scriptjs');
scriptjs('/js/ckeditor/ckeditor.js', function(){
  if(document.getElementById('discription')){
    CKEDITOR.replace('discription', {
      toolbar : [
        [
          'Bold',
          'Italic',
          '-',
          'NumberedList',
          'BulletedList',
          '-',
          'Link',
          'Unlink',
          '-',
          'Image',
          'Source'
        ],
      ]
    })
  }
});

$('.delete-message').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var id = button.data('id') ;
  var otype = button.data('otype') ;
  var modal = $(this);
  if (otype == 'layer') {
    modal.find('.modal-body').html('Вы уверены что хотите удалить слой #' + id + '</br> <b>ВНИМАНИЕ!</b> Это приведет к удалению обьектов слоя!' );
  }
  else{
    modal.find('.modal-body').text('Вы уверены что хотите удалить ' + otype + ' ' + id);
  }
  modal.find('.del').click(function() {
    window.location.href = '/admin/del/' + otype + '/' + id;
  });
})
