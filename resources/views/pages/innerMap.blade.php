<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Містобудівний Кадастр Миколаєва</title>
    <!-- Styles -->
    <link rel="stylesheet" href="/css/app.css">
    <!-- Scripts -->
    <script src="https://use.fontawesome.com/5dcdfa6cec.js"></script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app" style="width: 100%; height: 100%;">

        <google-map v-if="googleShow == true" ref='gmap' :center="center">
        @foreach ($zones as $key => $zone)
            <google-map-polygon
            v-if="showOrHide({{ $zone['layer_id'] }})"
            :paths="{{ $zone['geoData'] }}"
            :color="'#{{ $zone->layer->color }}'"
            :content="{ discription : '{{  $zone['discription'] }}' }"
            ></google-map-polygon>
        @endforeach
        @foreach ($objects as $key => $object)
            <google-map-marker
                v-if="showOrHide({{ $object['layer_id'] }})"
                :position="{{ $object['geoData'] }}"
                :clickable="true"
                :content="{ discription : '{{  $object['discription'] }}' }"
                @if (isset($object['icon']))
                  :icon="{url : 'images/pin/{{$object['icon']}}.png'}"
                @endif
              ></google-map-marker>
        @endforeach
        </google-map>

        <leaflet-map v-if="googleShow == false"
        >
        @foreach ($zones as $key => $zone)
            <v-polygon
            v-if="showOrHide({{ $zone['layer_id'] }})"
            :lat-lngs="<?php latlngToArray($zone['geoData']); ?>"
            :color="'#{{ $zone->layer->color }}'"
            :weight="0.3"
            ><v-popup :content="'{{ $zone['discription'] }}'" ></v-popup>
            </v-polygon>
        @endforeach
        @foreach ($objects as $key => $object)
            <v-marker
            v-if="showOrHide({{ $object['layer_id'] }})"
            :lat-lng="{{ $object['geoData'] }}"
            @if(isset($object['icon']))
            :icon="{{$object['icon']}}"
            @endif
            ><v-popup :content="'{{ $object['discription'] }}'" ></v-popup>
            </v-marker>
        @endforeach
        </leaflet-map>

    </div>
    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>
