@extends('layouts.app')

@section('content')
<style type="text/css">
nav {
  margin: 0!important;
}
</style>
<div id="map"></div>

<div class="panel-body tools_panel" id="tools_panel">
  <div class="btn-group" role="group" v-for="(button, index) in buttons" style="margin: 5px;">
    <a type="button" class="btn btn-default" :href="url">
      <i class="fa" aria-hidden="true" :class="button.icon"></i> @{{ button.title }}
    </a>
    <div class="btn-group" role="group">
      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu">
        <li v-for="layer in button.layers"><a :href="'{{ Request::fullUrl() }}&show=' + layer.val">@{{ layer.label }}</a></li>
      </ul>
    </div>
  </div>
  @if (isset($kadastrMap))
  <a type="button" class="btn btn-default" :href="urlk">
    Кадастровый слой
  </a>
  @endif
</div>

<script type="text/javascript">
new Vue({
  el: '#tools_panel',
  data: {
    buttons: {!! $request !!},
    kadastrMap: {!! $kadastrMap !!}
  },
  computed: {
    urlData: function () {
      var array = JSON.parse(JSON.stringify(this.buttons));
      return encodeURIComponent(JSON.stringify(array))
    },
    url: function () {
      return '/build/?data=' + this.urlData + '&kadastrMap=' + this.kadastrMap;
    },
    urlk: function () {
      return '/build/?data=' + this.urlData + '&kadastrMap=' + !this.kadastrMap;
    }
  },
});

$( document ).ready(setMapHeight);

$(function() {
    $(window).resize(setMapHeight);
});

function degrees2meters (lon,lat) {
        var x = lon * 20037508.34 / 180;
        var y = Math.log(Math.tan((90 + lat) * Math.PI / 360)) / (Math.PI / 180);
        y = y * 20037508.34 / 180;
        return [x, y]
}

function setMapHeight() {
  var width = $(window).width();
  var height = $(window).height();
  $('#map').height(height-50);
  $('#map').width(width);
}

function initMap() {
    var styledMapType = new google.maps.StyledMapType(
        [
            {
                "featureType": "administrative",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": "-100"
                    }
                ]
            },
            {
                "featureType": "administrative.province",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 65
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": "50"
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": "-100"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "all",
                "stylers": [
                    {
                        "lightness": "30"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "all",
                "stylers": [
                    {
                        "lightness": "40"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "hue": "#ffff00"
                    },
                    {
                        "lightness": -25
                    },
                    {
                        "saturation": -97
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels",
                "stylers": [
                    {
                        "lightness": -25
                    },
                    {
                        "saturation": -100
                    }
                ]
            }
        ],
    {name: 'Contrast'});

  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    center: {lat: 46.96153979413719, lng: 31.98896884918213},
    // mapTypeId: google.maps.MapTypeId.TERRAIN
    mapTypeControlOptions: {
            mapTypeIds: ['roadmap', 'terrain','styled_map']
          }
  });

    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');

@foreach ($zones as $key => $zone)

  var zone{{ $zone['id'] }} = new google.maps.Polygon({
    paths: {{ $zone['geoData'] }},
    strokeColor: '#{{ $zone->layer->color }}',
    strokeOpacity: 0.8,
    strokeWeight: 1,
    fillColor: '#{{ $zone->layer->color }}',
    fillOpacity: 0.35
  });

  zone{{ $zone['id'] }}.setMap(map);
  zone{{ $zone['id'] }}.addListener('click', showPopup{{ $zone['id'] }});

@endforeach

@foreach ($objects as $key => $object)

  var object{{ $object['id'] }} = new google.maps.Marker({
    position: {{ $object['geoData'] }},
    @if (isset($object['icon']))
    icon:  "img/pin/{{$object['icon']}}.png",
    @endif

  });

  object{{ $object['id'] }}.setMap(map);
  object{{ $object['id'] }}.addListener('click', showPopup{{ $object['id'] }});

@endforeach

  infoWindow = new google.maps.InfoWindow;
@if (isset($kadastrMap) && $kadastrMap=='true')
var WMSLayer = new google.maps.ImageMapType({
    getTileUrl: function (coord, zoom) {
        var proj = map.getProjection();
        var zfactor = Math.pow(2, zoom);
        // get Long Lat coordinates
        var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 256 / zfactor, coord.y * 256 / zfactor));
        var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 256 / zfactor, (coord.y + 1) * 256 / zfactor));
        //corrections for the slight shift of the SLP (mapserver)
        var deltaX = 0;
        var deltaY = 0;
        //create the Bounding box string
        var bbox = (degrees2meters(top.lng(),bot.lat())) + "," + degrees2meters(bot.lng(),top.lat());
        var url = "http://map.land.gov.ua/geowebcache/service/wms?tiled=true";
            url += "&REQUEST=GetMap"; //WMS operation
            url += "&SERVICE=WMS";    //WMS service
            url += "&VERSION=1.1.1";  //WMS version
            url += "&LAYERS=kadastr"; //WMS layers
            url += "&FORMAT=image/png" ; //WMS format
            url += "&TRANSPARENT=true";
            url += "&SRS=EPSG:3857";     //set EPSG:3857
            url += "&BBOX=" + bbox;      // set bounding box
            url += "&WIDTH=256";         //tile size in google
            url += "&HEIGHT=256";
        return url;                 // return URL for the tile
    },
    tileSize: new google.maps.Size(256, 256),
});
map.overlayMapTypes.push(WMSLayer);
@endif
}

@foreach ($zones as $key => $zone)
    function showPopup{{ $zone['id'] }}(event) {
      var contentString = '<div class="tultip_gmap"><b>#{{ $zone["id"] }} {{ $zone["name"] }}</b></br></br><?= $zone["discription"] ?><div>';
      infoWindow.setContent(contentString);
      infoWindow.setPosition(event.latLng);
      infoWindow.open(map);
    }
@endforeach
@foreach ($objects as $key => $object)
    function showPopup{{ $object['id'] }}(event) {

      var contentString = '<div class="tultip_gmap"><b>#{{ $object["id"] }} {{ $object["name"] }}</b></br></br><?= $object["discription"] ?><div>';
      infoWindow.setContent(contentString);
      infoWindow.setPosition(event.latLng);
      infoWindow.open(map);
    }
@endforeach
</script>
@endsection
