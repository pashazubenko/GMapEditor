@extends('layouts.app')
@section('content')

<div class="btn-toolbox toolbox ">
    <div class="btn-group">
        <a class="btn btn-default" @click="cats = []" role="button">Все</a>
        <div class="btn-group" role="group">
            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn btn-default dropdown-toggle">
                <i aria-hidden="true" class="fa fa-money"></i>
            </button>
            <ul class="dropdown-menu">
                <li><a @click="cats = [52,11,12,13,14]"><b>Інвестиційні об'єкти</b></a></li>
                <li><a @click="cats = [52]">Об'єкти</a></li>
                <li><a @click="cats = [11]">Industrial objects</a></li>
                <li><a @click="cats = [12]">Recreation objects</a></li>
                <li><a @click="cats = [13]">Community facilities</a></li>
                <li><a @click="cats = [14]">Electrical infrastructure</a></li>
            </ul>
        </div>
        <div class="btn-group" role="group">
            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle">
                <i aria-hidden="true" class="fa fa-building"></i>
            </button>
            <ul class="dropdown-menu">
                <li><a @click="cats = [15,16,17,18,19,20]"><b>Житлові зони</b></a></li>
                <li><a @click="cats = [15]">Зонинг Ж-1</a></li>
                <li><a @click="cats = [16]">Зонінг Ж-1п </a></li>
                <li><a @click="cats = [17]">Зонінг Ж-3</a></li>
                <li><a @click="cats = [18]">Зонінг Ж-4</a></li>
                <li><a @click="cats = [19]">Зонінг Ж-4п </a></li>
                <li><a @click="cats = [20]">Зонінг Ж-5 </a></li>
            </ul>
        </div>
        <div class="btn-group" role="group">
            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle">
                <i aria-hidden="true" class="fa fa-users"></i>
            </button>
            <ul class="dropdown-menu">
                <li><a @click="cats = [21]"><b>Громадські зони</b></a></li>
            </ul>
        </div>
        <div class="btn-group" role="group">
            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle"><i aria-hidden="true" class="fa fa-leaf"></i>
            </button>
            <ul class="dropdown-menu">
                <li><a @click="cats = [22,23,24,25,26,27,28]"><b>Рекреаційні зони</b></a></li>
                <li><a @click="cats = [22]">Зонінг Р-1 </a></li>
                <li><a @click="cats = [23]">Зонінг Р-1ік </a></li>
                <li><a @click="cats = [24]">Зонінг Р-2 </a></li>
                <li><a @click="cats = [25]">Зонінг Р-3</a></li>
                <li><a @click="cats = [26]">Зонінг Р-3пз</a></li>
                <li><a @click="cats = [27]">Зонінг Р-4</a></li>
                <li><a @click="cats = [28]">Зонінг Р-5</a></li>
            </ul>
        </div>
        <div class="btn-group" role="group">
            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle">
                <i aria-hidden="true" class="fa fa-sun-o"></i>
            </button>
            <ul class="dropdown-menu">
                <li><a @click="cats = [29]"><b>Курортні зони</b></a></li>
            </ul>
        </div>
        <div class="btn-group" role="group">
            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle">
                <i aria-hidden="true" class="fa fa-train"></i>
            </button>
            <ul class="dropdown-menu">
                <li><a @click="cats = [30,50,51]"><b>Зони транспортної інфраструктури</b></a></li>
                <li><a @click="cats = [30]">Зонінг ТР-1а</a></li>
                <li><a @click="cats = [50]">Зонінг ТР-1в</a></li>
                <li><a @click="cats = [51]">Зонінг ТР-1з</a></li>
            </ul>
        </div>
        <div class="btn-group" role="group">
            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle">
                <i aria-hidden="true" class="fa fa-cog"></i>
            </button>
            <ul class="dropdown-menu">
                <li><a @click="cats = [31,32,33]"><b>Зони інженерної інфраструктури</b></a></li>
                <li><a @click="cats = [31]">Зонінг ІН-1</a></li>
                <li><a @click="cats = [32]">Зонінг ІН-2-1</a></li>
                <li><a @click="cats = [33]">Зонінг ІН-2-2</a></li>
            </ul>
        </div>
        <div class="btn-group" role="group">
            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle">
                <i aria-hidden="true" class="fa fa-truck"></i>
            </button>
            <ul class="dropdown-menu">
                <li><a @click="cats = [34,35,36,37]"><b>Комунально-складські зони</b></a></li>
                <li><a @click="cats = [34]">Зонінг КС-2</a></li>
                <li><a @click="cats = [35]">Зонінг КС-3</a></li>
                <li><a @click="cats = [36]">Зонінг КС-4</a></li>
                <li><a @click="cats = [37]">Зонінг КС-5</a></li>
            </ul>
        </div>
        <div class="btn-group" role="group">
            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle">
                <i aria-hidden="true" class="fa fa-industry"></i>
            </button>
            <ul class="dropdown-menu">
                <li><a @click="cats = [38,39,40,41,42]"><b>Виробничі зони</b></a></li>
                <li><a @click="cats = [38]">Зонінг В-2</a></li>
                <li><a @click="cats = [39]">Зонінг В-3 </a></li>
                <li><a @click="cats = [40]">Зонінг В-4</a></li>
                <li><a @click="cats = [41]">Зонінг В-5</a></li>
                <li><a @click="cats = [42]">Зонінг ВКВ</a></li>
            </ul>
        </div>
        <div class="btn-group" role="group">
            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle">
                <i aria-hidden="true" class="fa fa-user-secret"></i>
            </button>
            <ul class="dropdown-menu">
                <li><a @click="cats = [43,44,46,47,48]"><b>Спеціальні зони</b></a></li>
                <li><a @click="cats = [43]">Зонінг С-1</a></li>
                <li><a @click="cats = [44]">Зонінг С-4</a></li>
                <li><a @click="cats = [46]">Зонінг С-4ж</a></li>
                <li><a @click="cats = [47]">Зонінг С-5</a></li>
                <li><a @click="cats = [48]">Зонінг С-6</a></li>
            </ul>
        </div>
        <div class="btn-group" role="group">
            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle">
                <i aria-hidden="true" class="fa fa-pagelines"></i>
            </button>
            <ul class="dropdown-menu">
                <li><a @click="cats = [54]"><b>Покіс трави ЖКГ</b></a></li>
            </ul>
        </div>
    </div>
    <div class="btn-group">
        <a class="btn btn-default" @click="googleShow=!googleShow" role="button" style="width: 61px;width: 60px;font-size: 10px;padding: 7px 6px;">OSM
            <img src="images/gmap.png" height="14" v-if="googleShow == true">
            <img src="images/osm.png"  height="14" v-if="googleShow == false">
        </a>
        <a class="btn btn-default" @click="kadastrMap($refs)" role="button" v-if="googleShow == true" style="width: 136px;font-size: 10px;padding: 7px 4px;overflow: hidden;">Кадастровий шар</a>
        <gmap-autocomplete :placeholder="'Пошук...'" @place_changed="setPlace" v-if="googleShow == true" style="width: 150px; font-size: 11px;"></gmap-autocomplete>
    </div>
</div>


<google-map
    v-if="googleShow == true"
    ref='gmap'
    :center="center"
    @openpopup="console.log('1')"
>
@foreach ($zones as $key => $zone)
    <google-map-polygon
    v-if="showOrHide({{ $zone['layer_id'] }})"
    :paths="{{ $zone['geoData'] }}"
    :color="'#{{ $zone->layer->color }}'"
    :content="{ discription : '{{  $zone['discription'] }}' }"
    ></google-map-polygon>
@endforeach
@foreach ($objects as $key => $object)
    <google-map-marker
        v-if="showOrHide({{ $object['layer_id'] }})"
        :position="{{ $object['geoData'] }}"
        :opened="false"
        :content="{ discription : '{{  $object['discription'] }}' }"
        @if (isset($object['icon']))
          :icon="{url : 'images/pin/{{$object['icon']}}.png'}"
        @endif
      ></google-map-marker>
@endforeach
</google-map>

<leaflet-map v-if="googleShow == false"
>
@foreach ($zones as $key => $zone)
    <v-polygon
    v-if="showOrHide({{ $zone['layer_id'] }})"
    :lat-lngs="<?php latlngToArray($zone['geoData']); ?>"
    :color="'#{{ $zone->layer->color }}'"
    :weight="0.3"
    ><v-popup :content="'{{ $zone['discription'] }}'" ></v-popup>
    </v-polygon>
@endforeach
@foreach ($objects as $key => $object)
    <v-marker
    v-if="showOrHide({{ $object['layer_id'] }})"
    :lat-lng="{{ $object['geoData'] }}"
    @if(isset($object['icon']))
    :icon="{{$object['icon']}}"
    @endif
    ><v-popup :content="'{{ $object['discription'] }}'" ></v-popup>
    </v-marker>
@endforeach
</leaflet-map>


@endsection
