@extends('layouts.app')

@section('content')
<div class="container-fluid">
<!-- CREATE HEADER -->
<!-- <div class="row">
  <div class="col-md-10 col-md-offset-1">
    <ol class="breadcrumb">
      <li><a href="#">Главная</a></li>
      <li class="active">Админ панель</li>
    </ol>
  </div>
</div> -->
<div class="page-header">
  @if(isset($objects))
  <h1>Все обьекты</h1>
  @endif
  @if(isset($zones))
    <h1>Все зоны</h1>
  @endif
  @if(isset($layers))
    <h1>Все слои</h1>
  @endif
</div>

@if (session('message'))
    <div class="alert alert-info" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ session('message') }}
    </div>
@endif

@if (session('warning'))
    <div class="alert alert-danger" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ session('warning') }}
    </div>
@endif

<!-- LAST -->
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-primary">
        @if(isset($objects))
          <div class="panel-heading">
            <h3 class="panel-title">
              <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                <b>Обьекты</b> <span class="badge">{{$objects_count}}</span>
              </h3>
          </div>
          <div class="table-responsive">
              <table class="table table-striped table-hover table-condensed">
                <thead>
                  <tr>
                    <th><i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></th>
                    <th><i class="fa fa-font" aria-hidden="true"></i> Название</th>

                    <th><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Категория</th>
                    <th><span class="glyphicon glyphicon glyphicon-calendar" aria-hidden="true"></span> Дата Создания</th>
                    <th><span class="glyphicon glyphicon glyphicon-calendar" aria-hidden="true"></span> Дата Изменения</th>
                    <th>
                      <span class="glyphicon glyphicon-cog" aria-hidden="true" style="width: 50px; text-align: center;"></span>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($objects as $object)
                  <tr>
                    <th scope="row"> {{ $object->id }}</th>
                    <td>{{ $object->name }}</td>
                    <td style="color:#{{ $object->layer->color }} ;">{{ $object->layer->name }}</td>
                    <td>{{ $object->created_at }}</td>
                    <td>{{ $object->updated_at }}</td>
                    <td>
                      <div class="btn-group btn-group-xs" role="group">
                        <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-otype="object" data-id="{{ $object->id }}"  data-target=".delete-message">
                          <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </button>
                        <a class="btn btn-primary" href="{{ url('/admin/edit-point/' . $object->id) }}">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                      </a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>

          @endif

          @if(isset($zones))
            <div class="panel-heading">
              <h3 class="panel-title">
                <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                <b>Зоны</b> <span class="badge">{{$zones_count}}</span>
              </h3>
            </div>
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th><i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></th>
                  <th><i class="fa fa-font" aria-hidden="true"></i> Название</th>
                  <th><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Категория</th>
                  <th><span class="glyphicon glyphicon glyphicon-calendar" aria-hidden="true"></span> Дата Создания</th>
                  <th><span class="glyphicon glyphicon glyphicon-calendar" aria-hidden="true"></span> Дата Изменения</th>
                  <th>
                    <span class="glyphicon glyphicon-cog" aria-hidden="true" style="width: 50px; text-align: center;"></span>
                  </th>
                </tr>
              </thead>
              <tbody>
              @foreach ($zones as $zone)
                <tr>
                  <th scope="row"> {{ $zone->id }}</th>
                  <td>{{ $zone->name }}</td>
                  <td style="color: #{{ $zone->layer->color }};" >{{ $zone->layer->name }}</td>
                  <td>{{ $zone->created_at }}</td>
                  <td>{{ $zone->updated_at }}</td>
                  <td>
                    <div class="btn-group btn-group-xs" role="group">
                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-otype="zone" data-id="{{ $zone->id }}"  data-target=".delete-message">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                      </button>
                      <a class="btn btn-primary" href="{{ url('/admin/edit-geo/' . $zone->id) }}">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                      </a>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @endif

          @if(isset($layers))
            <div class="panel-heading">
              <h3 class="panel-title">
                <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                <b>Слои</b> <span class="badge">{{$layers_count}}</span>
              </h3>
            </div>
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th><i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></th>
                  <th><i class="fa fa-font" aria-hidden="true"></i> Название</th>
                  <th><i class="fa fa-cube" aria-hidden="true"></i> Цвет</th>
                  <th><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Иконка</th>
                  <th><span class="glyphicon glyphicon glyphicon-calendar" aria-hidden="true"></span> Дата Создания</th>
                  <th><span class="glyphicon glyphicon glyphicon-calendar" aria-hidden="true"></span> Дата Изменения</th>
                  <th>
                    <span class="glyphicon glyphicon-cog" aria-hidden="true" style="width: 50px; text-align: center;"></span>
                  </th>
                </tr>
              </thead>
              <tbody>
              @foreach ($layers as $layer)
                <tr>
                  <th scope="row"> {{ $layer->id }}</th>
                  <td>{{ $layer->name }}</td>
                  <td style="color: #{{ $layer->color }};" >#{{ $layer->color }}</td>
                  <td>{{ $layer->icon }}</td>
                  <td>{{ $layer->created_at }}</td>
                  <td>{{ $layer->updated_at }}</td>
                  <td>
                    <div class="btn-group btn-group-xs" role="group">
                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-otype="layer" data-id="{{ $layer->id }}" data-target=".delete-message">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                      </button>
                      <a class="btn btn-primary" href="{{ url('/admin/edit-layer/' . $layer->id) }}">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                      </a>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @endif




      </div>
  </div>
</div>

<!-- DELETE MODAL -->
<div class="modal fade delete-message" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><b>Удаление</b></h4>
      </div>
      <div class="modal-body">
        <p>Вы уверены что хотите удалить элемент?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
        <button type="button" class="btn del btn-danger">Удалить</button>
      </div>
    </div>
  </div>
</div>
<!-- /DELETE MODAL -->
</div>
@endsection
