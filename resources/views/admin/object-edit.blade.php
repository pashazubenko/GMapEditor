@extends('layouts.app')

@section('content')
<div class="container-fluid">
<div class="page-header">
  <h1>Панель управления объектами</h1>
</div>

<div class="col-md-12">

@if (isset($errors))
  @foreach ($errors->messages() as $key=>$value)
    <!-- {{ $key }}  -->
    @foreach ($value as $key=>$value)
      <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <b>Ошибка ввода данных:</b> {{ $value }}
      </div>
    @endforeach
  @endforeach
@endif

</div>
<div class="row">
<form method="POST" action="{{ url('/admin/edit-point') }}"  class="form">
                  {{ csrf_field() }}
                  <input  type="hidden" name="id" value="{{ isset($request['id']) ? $request['id'] : '' }}"/>
        <div class="col-md-8">
            <div class="panel panel-info">
                <div class="panel-heading">
                  <h3 class="panel-title">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                    <b>Карта</b>
                  </h3>
                </div>
                <div class="panel-body">
                  <google-map-point-editor
                    :geojson = <?php if (isset($request['geoData'])){ echo $request['geoData']; } else { echo '0';} ?>
                    ></google-map-point-editor>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-success">
                <div class="panel-heading">
                  <h3 class="panel-title">
                    <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                    <b>Создать объект</b>
                  </h3>
                </div>
                  <div class="panel-body">
                    <div class="form-group">
                      <label for="name">Имя</label>
                      <input  class="form-control" name="name" value="{{ isset($request['name']) ? $request['name'] : '' }}"/>

                      <label for="layer">Cлой</label>
                      <select class="form-control" name="layer" >
                      @if (isset($layers))
                        @foreach ($layers as $key=>$value)
                          <option value="{{ $value['id'] }}" >{{ $value['name'] }}</option>
                        @endforeach
                      @endif
                      </select>

                      <label for="exampleInputFile">Иконка</label>
                      <div class="form-group">
                        <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default">
                          <input type="radio" name="icon" value="accomodation">
                          <img class="icons" src="/images/pin/accomodation.png">
                        </label>
                        <label class="btn btn-default ">
                          <input type="radio" name="icon" value="electricity">
                          <img class="icons" src="/images/pin/electricity.png">
                        </label>
                        <label class="btn btn-default ">
                          <input type="radio" name="icon" value="gas">
                          <img class="icons" src="/images/pin/gas.png">
                        </label>
                        <label class="btn btn-default ">
                          <input type="radio" name="icon" value="industrial">
                          <img class="icons" src="/images/pin/industrial.png">
                        </label>
                        <label class="btn btn-default ">
                          <input type="radio" name="icon" value="recreation">
                          <img class="icons" src="/images/pin/recreation.png">
                        </label>
                        <label class="btn btn-default ">
                          <input type="radio" name="icon" value="sewage">
                          <img class="icons" src="/images/pin/sewage.png">
                        </label>
                        <label class="btn btn-default ">
                          <input type="radio" name="icon" value="water">
                          <img class="icons" src="/images/pin/water.png">
                        </label>
                        </div>
                      </div>

                      <label for="discription">Описание</label>
                      <textarea  class="form-control" id="discription" rows="5" cols="80" name="discription">{{ isset($request['discription']) ? $request['discription'] : '' }}</textarea>
                    </div>
                    <div class="alert alert-danger" role="alert">Избегайте использование одинарных кавычек! <kbd>\'</kbd> - экранируйте их! </div>
                </div>
                <div class="panel-footer">
                  <button type="submit" class="btn btn-block btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Сохранить</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
