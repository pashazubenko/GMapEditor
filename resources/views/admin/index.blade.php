@extends('layouts.app')

@section('content')
<div class="container-fluid" style="margin-top:20px">
<!-- CREATE HEADER -->
<!-- <div class="row">
  <div class="col-md-10 col-md-offset-1">
    <ol class="breadcrumb">
      <li><a href="#">Главная</a></li>
      <li class="active">Админ панель</li>
    </ol>
  </div>
</div> -->

<div class="row">
  <div class="col-md-4">
      <div class="panel panel-success">
          <div class="panel-heading"><h2 class="panel-title"><b>Добавить зону</b></h2></div>
          <div class="panel-body">
            <a href="{{ url('/admin/edit-geo') }}"><span class="glyphicon glyphicon-object-align-top" aria-hidden="true" style=" font-size: 120px; margin: 0 auto; text-align:center; width: 100%; color: #3c763d;"></span></a>
          </div>
      </div>
  </div>
  <div class="col-md-4">
      <div class="panel panel-success">
          <div class="panel-heading"><h2 class="panel-title"><b>Добавить слой</b></h2></div>
          <div class="panel-body">
            <a href="{{ url('/admin/edit-layer') }}"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true" style=" font-size: 120px; margin: 0 auto; text-align:center; width: 100%; color: #3c763d;"></span></a>
          </div>
      </div>
  </div>
  <div class="col-md-4">
      <div class="panel panel-success">
          <div class="panel-heading"><h2 class="panel-title"><b>Добавить обьект</b></h2></div>
          <div class="panel-body">
            <a href="{{ url('/admin/edit-point') }}"><span class="glyphicon glyphicon-map-marker" aria-hidden="true" style=" font-size: 120px; margin: 0 auto; text-align:center; width: 100%; color: #3c763d;"></span></a>
          </div>
      </div>
  </div>
</div>
<div class="row">
<div class="col-md-12">
  <div class="panel panel-success">
    <div class="panel-heading">
      <h2 class="panel-title">
        <b>Редактор меню</b>

      <a class="btn btn-success btn-xs pull-right" href="{{ url('/admin/builder') }}" role="button">
        Изменить
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      </a>
      </h2>
    </div>
  </div>
</div>
</div>
<!-- / CREATE HEADER -->

@if (session('warning'))
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>{{ session('warning') }}</strong>
    </div>
@endif




<!-- LAST -->
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">
              <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                <b>Слои</b> <span class="badge">{{$layers_count}}</span>
                <button type="button" class="btn btn-xs btn-success pull-right" data-toggle="modal" data-otype="object" data-id=""  data-target=".import">
                  <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                </button>
              </h3>
            </div>
          @if(sizeof($layers))
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th><i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></th>
                  <th><i class="fa fa-font" aria-hidden="true"></i> Название</th>
                  <th><i class="fa fa-cube" aria-hidden="true"></i> Цвет</th>
                  <th><span class="glyphicon glyphicon glyphicon-calendar" aria-hidden="true"></span> Дата Создания</th>
                  <th><span class="glyphicon glyphicon glyphicon-calendar" aria-hidden="true"></span> Дата Изменения</th>
                  <th>
                    <span class="glyphicon glyphicon-cog" aria-hidden="true" style="width: 50px; text-align: center;"></span>
                  </th>
                </tr>
              </thead>
              <tbody>
              @foreach ($layers as $layer)
                <tr>
                  <th scope="row"> {{ $layer->id }}</th>
                  <td>{{ $layer->name }}</td>
                  <td style="color: #{{$layer->color }};" >#{{$layer->color }}</td>
                  <td>{{ $layer->created_at }}</td>
                  <td>{{ $layer->updated_at }}</td>
                  <td>
                    <div class="btn-group btn-group-xs" role="group">
                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-otype="layer" data-id="{{ $layer->id }}" data-target=".delete-message">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                      </button>
                      <a class="btn btn-primary" href="{{ url('/admin/edit-layer/' . $layer->id) }}">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                      </a>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @else
            <div class="panel-body">
              <blockquote>
                <p>Всё, что пусто, легко управляемо.</p>
                <footer>Бесконечная история <cite title="Source Title">Михаэль Энде</cite></footer>
              </blockquote>
            </div>
          @endif
          @if(sizeof($layers))
          <div class="panel-footer">
            <div class="btn-group btn-group-sm" role="group" aria-label="...">
                <a class="btn btn-success" href="{{ url('/admin/edit-layer') }}" role="button">
                  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
                <a class="btn btn-primary" href="{{ url('/admin/listall/layer') }}" role="button">Смотреть Все
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                </a>
            </div>
          </div>
          @endif
      </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">
              <span class="glyphicon glyphicon-object-align-top" aria-hidden="true"></span>
              <b>Зоны</b> <span class="badge">{{$zones_count}}</span>
              <button type="button" class="btn btn-xs btn-success pull-right" data-toggle="modal" data-otype="object" data-id=""  data-target=".import">
                <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
              </button>
            </h3>
          </div>
          @if(sizeof($zones))
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th><i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></th>
                  <th><i class="fa fa-font" aria-hidden="true"></i> Название</th>
                  <th><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Категория</th>
                  <th><span class="glyphicon glyphicon glyphicon-calendar" aria-hidden="true"></span> Дата Создания</th>
                  <th><span class="glyphicon glyphicon glyphicon-calendar" aria-hidden="true"></span> Дата Изменения</th>
                  <th>
                    <span class="glyphicon glyphicon-cog" aria-hidden="true" style="width: 50px; text-align: center;"></span>
                  </th>
                </tr>
              </thead>
              <tbody>
              @foreach ($zones as $zone)
                <tr>
                  <th scope="row"> {{ $zone->id }}</th>
                  <td>{{ $zone->name }}</td>
                  <td style="color: #{{ $zone->layer->color }};" >{{ $zone->layer->name }}</td>
                  <td>{{ $zone->created_at }}</td>
                  <td>{{ $zone->updated_at }}</td>
                  <td>
                    <div class="btn-group btn-group-xs" role="group">
                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-otype="zone" data-id="{{ $zone->id }}"  data-target=".delete-message">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                      </button>
                      <a class="btn btn-primary" href="{{ url('/admin/edit-geo/' . $zone->id) }}">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                      </a>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @else
            <div class="panel-body">
              <blockquote>
                <p>Всё, что пусто, легко управляемо.</p>
                <footer>Бесконечная история <cite title="Source Title">Михаэль Энде</cite></footer>
              </blockquote>
            </div>
          @endif
          @if(sizeof($zones))
          <div class="panel-footer">
            <div class="btn-group btn-group-sm" role="group" aria-label="...">
                <a class="btn btn-success" href="{{ url('/admin/edit-geo') }}" role="button">
                  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
                <a class="btn btn-primary" href="{{ url('/admin/listall/zone') }}" role="button">Смотреть Все
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                </a>
            </div>
          </div>
          @endif
      </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">
              <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                <b>Обьекты</b> <span class="badge">{{$objects_count}}</span>
                <button type="button" class="btn btn-xs btn-success pull-right" data-toggle="modal" data-otype="object" data-id=""  data-target=".import">
                  <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                </button>
            </h3>
          </div>
          @if(sizeof($objects))
          <div class="table-responsive">
              <table class="table table-striped table-hover table-condensed">
                <thead>
                  <tr>
                    <th><i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></th>
                    <th><i class="fa fa-font" aria-hidden="true"></i> Название</th>

                    <th><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Категория</th>
                    <th><span class="glyphicon glyphicon glyphicon-calendar" aria-hidden="true"></span> Дата Создания</th>
                    <th><span class="glyphicon glyphicon glyphicon-calendar" aria-hidden="true"></span> Дата Изменения</th>
                    <th>
                      <span class="glyphicon glyphicon-cog" aria-hidden="true" style="width: 50px; text-align: center;"></span>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($objects as $object)
                  <tr>
                    <th scope="row"> {{ $object->id }}</th>
                    <td>{{ $object->name }}</td>
                    <td style="color:#{{ $object->layer->color }} ;">{{ $object->layer->name }}</td>
                    <td>{{ $object->created_at }}</td>
                    <td>{{ $object->updated_at }}</td>
                    <td>
                      <div class="btn-group btn-group-xs" role="group">
                        <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-otype="object" data-id="{{ $object->id }}"  data-target=".delete-message">
                          <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </button>
                        <a class="btn btn-primary" href="{{ url('/admin/edit-point/' . $object->id) }}">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                      </a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
          @else
            <div class="panel-body">
              <p>
                <p>"Всё, что пусто, легко управляемо."</p>
                <i> -- Бесконечная история <b>Михаэль Энде</b></i>
              </p>
            </div>
          @endif
          @if(sizeof($objects))
          <div class="panel-footer">
            <div class="btn-group btn-group-sm" role="group" aria-label="...">
                <a class="btn btn-success" href="{{ url('/admin/edit-point') }}" role="button">
                  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
                <a class="btn btn-primary" href="{{ url('/admin/listall/object') }}" role="button">Смотреть Все
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                </a>
            </div>
          </div>
          @endif
      </div>
  </div>
</div>

<!-- DELETE MODAL -->
<div class="modal fade delete-message" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><b>Удаление</b></h4>
      </div>
      <div class="modal-body">
        <p>Вы уверены что хотите удалить элемент?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
        <button type="button" class="btn del btn-danger">Удалить</button>
      </div>
    </div>
  </div>
</div>

<!-- Import MODAL -->
<div class="modal fade import" tabindex="-1" role="dialog" aria-labelledby="myLargeImport">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><b>Импорт</b></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="exampleInputFile">Фаил импорта:</label>
          <input type="file" id="exampleInputFile">
          <p class="help-block">Перед загрузкой убедитесь в валидности вашего *.json файла</p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-right">Загрузить</button>
      </div>
    </div>
  </div>
</div>

</div>
@endsection
