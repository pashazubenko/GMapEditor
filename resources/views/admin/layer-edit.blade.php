@extends('layouts.app')

@section('content')
<div class="container-fluid">
<div class="page-header">
  <h1>Панель управления слоями</h1>
</div>
<div class="row">
  <div class="col-md-12">


@if (isset($errors))
  @foreach ($errors->messages() as $key=>$value)
    <!-- {{ $key }}  -->
    @foreach ($value as $key=>$value)
      <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <b>Ошибка ввода данных:</b> {{ $value }}
      </div>
    @endforeach
  @endforeach
@endif

  <form method="POST" action="{{ url('/admin/edit-layer') }}"  class="form">
  {{ csrf_field() }}

      <div class="panel panel-success">
          <div class="panel-heading">
            <h3 class="panel-title">
              <i class="fa fa-info" aria-hidden="true"></i>
              <b>Info</b>
            </h3>
          </div>
          <div class="panel-body">
            Слой - обьеденение обьектов карты одной тематикой(признаком).
            Обьекты одного слоя будут иметь один цвет для зон и точек карты.
          </div>

          <div class="panel-heading">
            <h3 class="panel-title">
              <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
              <b>Создать слой</b>
            </h3>
          </div>

          <div class="panel-body">
              <input  type="hidden" name="id" value="{{ isset($request['id']) ? $request['id'] : '' }}" />
              <div class="form-group">
                <label for="name">Имя</label><input  class="form-control" name="name" value="{{ isset($request['name']) ? $request['name'] : '' }}" />
              </div>

              <div class="form-group">
                <label for="discription">Описание</label>
                <textarea  class="form-control" id="discription" rows="4" name="discription">{{ isset($request['discription']) ? $request['discription'] : '' }}</textarea>
              </div>

              <div class="form-group">
                <label for="layer">Цвет</label>
                <p class="help-block">
                  <slider-picker :value="{hex: '#194d33'}" @input="updateColorValue"></slider-picker>
                </p>
                <div class="input-group">
                  <span class="input-group-addon" >#</span>
                  <input type="text" class="form-control" id="colorpic" ref="colorpic" placeholder="FF0055" name="color" value="{{ isset($request['color']) ? $request['color'] : '' }}">
                </div>
              </div>

          </div>
          <div class="panel-footer">
             <button type="submit" class="btn btn-block btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Сохранить</button>
          </div>
      </div>
      </form>
  </div>
</div>
</div>
@endsection
